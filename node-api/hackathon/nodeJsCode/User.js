var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://127.0.0.1:27017/test');
var User = new mongoose.Schema({
      	username: String,
      	email: String,
 	password: String,
 	date : { type: Date, default: Date.now },
	type: Number
    });
 module.exports = db.model('User_collection',User);
