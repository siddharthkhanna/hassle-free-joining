var Sequelize = require('sequelize');
var sqlz = new Sequelize(undefined,undefined,undefined,{
	'dialect':'sqlite',
	'storage':__dirname + '/database/basic-sqlite-database.sqlite'
});

var db = {};
db.user = sqlz.import(__dirname + '/models/user.js');
db.sqlz = sqlz;
db.Sequelize = Sequelize;
module.exports = db;