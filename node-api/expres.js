var exp = require('express');
var app = exp();
var db = require('./db.js');
var bodyparser = require('body-parser');
var cookieParser = require('cookie-parser');
var crypto = require('crypto-js');
var secretKey = '123yut';
var shortid = require('shortid');
app.use(cookieParser());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
  extended:true
}));

app.use(exp.static(__dirname+"/public"));


app.get("/welcome/:token",function(req,res){
	res.send("Hello World");
	});


app.post("/registerUser",function(req,res){
	
  var uuuid = shortid.generate();
  console.log(req.body.username+"**");
  var username = req.body.username;
  var password = req.body.password;
  var email = req.body.email;
  var type = req.body.Usertype;
  var uid = crypto.HmacSHA1(uuuid,secretKey).toString();
  
  var userObj = {
    "uid":uid,
    "username":username,
    "emailid":email,
    "password":password,
    "userType":type
      };
      
  db.user.create(userObj)

  .then(function(user){
    console.log("Created");
    if(user){
        
        var mailer = require('./lib/sendMail');
        var from = "hasslefreeonboarding@gmail.com";
        var to = user.emailid;
        var url = "http://localhost:3000/jobseeker.html";
        var msg = "<html><body>Welcome to Onboarding Service. Ensuring hassle free on boarding for you.<br><br> You have been sucessfully registered as, <br><br> Username: "+user.username+"<br><p>Password: "+user.password+"</p><br>";
        var finalmag = msg+'<a href=\"'+url+"\">"+url.toString()+"</a></body></html>";
        console.log(finalmag);
        var msg = mailer.sendMail(to,from,finalmag);
         res.cookie('user',user.username,{
          maxAge:900000
        });
         res.cookie('uid',user.uid,{
          maxAge:900000
        });
          res.cookie('type',user.userType,{
          maxAge:900000
        });
        
        res.redirect('MainPanel.html');
    }
    else{
     console.log("dead");
    
    res.status(200);}
  })

  .catch(function(e){
    res.status(400).json(e);
    console.log(e);
  });
  

});

// app.get("/userData/:userid",function(req,res){
//  var userid = req.params.userid;
//   db.user.findOne({
//     where:{
//       uid:userid;
//     }
//   }).then(function(user){
//     res.send()
//   });
// });

app.post("/welcomeUserJ",function(req,res){
    console.log("Login Authentication");

    var username = req.body.username;
    var pasword = req.body.password;
    db.user.findOne({
      where:{
      emailid:username,
      password:pasword
    }
    })
    .then(function(user){
      console.log(user);
      if(user){
        res.cookie('user',user.username,{
          maxAge:900000
        });
         res.cookie('uid',user.uid,{
          maxAge:900000
        });
         res.cookie('type',user.userType,{
          maxAge:900000
        });
        res.redirect('MainPanel.html');
      

      }else{
        res.cookie('error','404',{
          maxAge:900000
        });
        res.redirect('jobseeker.html');
      }
    })
    .catch(function(e){
      res.status(500).send();
    });
    console.log("Auth Ended");
});



app.post("/sendMail",function(req,res){

  var mailer = require('./lib/sendMail');
  var from = "hasslefreeonboarding@gmail.com";
  var to = req.body.to;
  //var fromUser = req.body.from;


  db.user.findOne({
      where:{
      emailid:to,
      userType:'1'
    }
    })
    .then(function(user){
      
      if(user){
        var uuid = user.uid;
        var username = user.username;  
        console.log(user.uid);
      }else{
        
        res.redirect('formdisplay.html#RecruiternotFound');
      }
    })
    .catch(function(e){
      res.status(500).send();
    });
    console.log("Auth Ended");


  var tokenId = uuid +":"+username;
  var url = "http://localhost:3000/jobseeker.html/" + crypto.HmacSHA1(tokenId,secretKey).toString();
  var msg = "<html><body>Welcome to Onboarding Service. Ensuring hassle free on boarding for you.<br><br> User has uploaded his/her documents, <br><br>please link the below url to access documents now!<br><br>";
  var finalmag = msg+'<a href=\"'+url+"\">"+url.toString()+"</a></body></html>";
  console.log(finalmag);
  var msg = mailer.sendMail(to,from,finalmag);
  res.send(msg);
});

db.sqlz.sync().then(function(){

  app.listen("3000",function(){
  console.log("Express is listening");
  });

});

