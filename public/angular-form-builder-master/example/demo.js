(function() {
  angular.module('app', ['builder', 'builder.components', 'validator.rules']).run([
    '$builder', function($builder) {
      $builder.registerComponent('sampleInput', {
        group: 'from html',
        label: 'Sample',
        description: 'From html template',
        placeholder: 'placeholder',
        required: false,
        validationOptions: [
          {
            label: 'none',
            rule: '/.*/'
          }, {
            label: 'number',
            rule: '[number]'
          }, {
            label: 'email',
            rule: '[email]'
          }, {
            label: 'url',
            rule: '[url]'
          }
        ],
        templateUrl: 'example/template.html',
        popoverTemplateUrl: 'example/popoverTemplate.html'
      });
      return $builder.registerComponent('name', {
        group: 'Default',
        label: 'Name',
        required: false,
        arrayToText: true,
        template: "<div class=\"form-group\">\n    <label for=\"{{formName+index}}\" class=\"col-md-4 control-label\" ng-class=\"{'fb-required':required}\">{{label}}</label>\n    <div class=\"col-md-8\">\n        <input type='hidden' ng-model=\"inputText\" validator-required=\"{{required}}\" validator-group=\"{{formName}}\"/>\n        <div class=\"col-sm-6\" style=\"padding-left: 0;\">\n            <input type=\"text\"\n                ng-model=\"inputArray[0]\"\n                class=\"form-control\" id=\"{{formName+index}}-0\"/>\n            <p class='help-block'>First name</p>\n        </div>\n        <div class=\"col-sm-6\" style=\"padding-left: 0;\">\n            <input type=\"text\"\n                ng-model=\"inputArray[1]\"\n                class=\"form-control\" id=\"{{formName+index}}-1\"/>\n            <p class='help-block'>Last name</p>\n        </div>\n    </div>\n</div>",
        popoverTemplate: "<form>\n    <div class=\"form-group\">\n        <label class='control-label'>Label</label>\n        <input type='text' ng-model=\"label\" validator=\"[required]\" class='form-control'/>\n    </div>\n    <div class=\"checkbox\">\n        <label>\n            <input type='checkbox' ng-model=\"required\" />\n            Required\n        </label>\n    </div>\n\n    <hr/>\n    <div class='form-group'>\n        <input type='submit' ng-click=\"popover.save($event)\" class='btn btn-primary' value='Save'/>\n        <input type='button' ng-click=\"popover.cancel($event)\" class='btn btn-default' value='Cancel'/>\n        <input type='button' ng-click=\"popover.remove($event)\" class='btn btn-danger' value='Delete'/>\n    </div>\n</form>"
      });
    }
  ]).controller('DemoController', [
    '$scope', '$builder', '$validator', function($scope, $builder, $validator) {
      var checkbox, textbox;
      //textbox = $builder.addFormObject('default', {
      //  id: 'textbox',
      //  component: 'textInput',
      //  label: 'Name',
      //  description: 'Your name',
      //  placeholder: 'Your name',
      //  required: true,
      //  editable: false
      //});
      //checkbox = $builder.addFormObject('default', {
      //  id: 'checkbox',
      //  component: 'checkbox',
      //  label: 'Pets',
      //  description: 'Do you have any pets?',
      //  options: ['Dog', 'Cat']
      //});
      //$builder.addFormObject('default', {
      //  component: 'sampleInput'
      //});
      $scope.form = $builder.forms['default'];
      $scope.input = [];
      $scope.defaultValue = {};
      //$scope.defaultValue[textbox.id] = 'default value';
      //$scope.defaultValue[checkbox.id] = [true, true];
      return $scope.submit = function() {
        return $validator.validate($scope, 'default').success(function() {
            $.ajax({
                method: "POST",
                url: "http://localhost:3000/setForm",
                data: { 'dataform' : JSON.stringify($scope.form) },
                success: function(){
                  window.location.href = 'http://localhost:3000/ViewForms.html';
                }
            });
            //return console.log($scope.input);
        }).error(function() {
          return console.log('error');
        });
      };
    }
  ]).controller('ShowFormController', [
      '$scope', '$builder', '$validator', function($scope, $builder, $validator) {
          var checkbox, textbox;
          var getUrlParams = Array();
          getQueryVariable();
          function getQueryVariable() {
              var query = window.location.search.substring(1);
              var vars = query.split("&");
              for (var i=0;i<vars.length;i++) {
                  var pair = vars[i].split("=");
                  getUrlParams[pair[0]] = pair[1];
              }

          }
          $.ajax({
              url: "http://localhost:3000/getForm",
              method: "GET",
              headers: {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Content-Type", "Access-Control-Allow-Methods": "GET, POST, PUT"},
              dataType: "jsonp",
              data: {uid: getUrlParams['uid'], id: getUrlParams['id']},
              success:function(result) {
                  result = JSON.parse(result.form);
                  for (var key in result) {
                      data = result[key];
                      makeElement(data);
                  }
                  $scope.form = $builder.forms['default'];
                  $scope.input = [];
                  $scope.defaultValue = {};
                  //$scope.defaultValue[textbox.id] = 'default value';
                  //console.log(result);
                  //return $validator.validate($scope, 'default');
                  return $scope.submit = function() {
                      return $validator.validate($scope, 'default').success(function() {
                          $.ajax({
                              method: "POST",
                              url: "http://localhost:3000/insertUserForm",
                              data: { 'dataform' : $scope.input, 'fullformValues': $scope.form, id: getUrlParams['id']},
                              success: function(result) {
                                  alert("We got your data. Thanks!!!");
                              }
                          });
                          //return console.log($scope.input);
                      }).error(function() {
                          return console.log('error');
                      });
                  };
              }, error: function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus, errorThrown);
              }
          });

          function makeElement(data) {
              var options = data.options;
              data.component = $builder.addFormObject('default', {
                  id: data.id,
                  component: data.component,
                  label: data.label,
                  description: data.description,
                  placeholder: data.placeholder,
                  required: data.required,
                  editable: data.editable,
                  options: options
              });
          }
      }
  ]).controller('FileUploadController', [
      '$scope', '$builder', '$validator', function($scope, $builder, $validator) {
          $scope.uploadVideo = function () {
              var photo = document.getElementById("photo");
              var file = photo.files[0];
              var fd = new FormData();  //Create FormData object
              fd.append('file', file);

              $.ajax({
                  method: "POST",
                  url: "http://localhost:3000/uploadForm",
                  data: { 'file' : fd }
              });
              //
              //$http.post('/uploadForm', fd, {
              //    transformRequest: angular.identity,
              //    headers: {'Content-Type': undefined}
              //}).success(function (data) {
              //    // Do your work
              //});
          };
      }
  ]).controller('GetFilledUserForm', [
      '$scope', '$builder', '$validator', function($scope, $builder, $validator) {
          var getUrlParams = Array();
          getUrlParams = getQueryVariable(getUrlParams);
          $.ajax({
              url: "http://localhost:3000/getFilledUserForm",
              method: "GET",
              headers: {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Content-Type", "Access-Control-Allow-Methods": "GET, POST, PUT"},
              dataType: "jsonp",
              data: {'filledFormId': getUrlParams['filledFormId']},
              success:function(result) {
                  result = JSON.parse(result);
                  for (var key in result) {
                      data = result[key];
                      makeElement(data);
                  }
                  $scope.form = $builder.forms['default'];
                  $scope.input = [];
                  $scope.defaultValue = {};
                  //$scope.defaultValue[textbox.id] = 'default value';
                  //console.log(result);
                  //return $validator.validate($scope, 'default');
                  return $scope.submit = function() {
                      return $validator.validate($scope, 'default').success(function() {
                          $.ajax({
                              method: "POST",
                              url: "http://localhost:3000/insertUserForm",
                              data: { 'dataform' : $scope.input, 'fullformValues': $scope.form }
                          });
                          //return console.log($scope.input);
                      }).error(function() {
                          return console.log('error');
                      });
                  };
              }, error: function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus, errorThrown);
              }
          });

          function makeElement(data) {
              var options = data.options;
              if (data.component == "checkbox" || data.component == "select" || data.component == "radio") {
                  data.component = "textInput";
              }
              data.component = $builder.addFormObject('default', {
                  id: data.id,
                  component: data.component,
                  label: data.label,
                  description: data.description,
                  placeholder: data.value,
                  required: data.required,
                  editable: false
              });
          }
      }
  ]).controller('ViewAllFormsFilledByUser', [
      '$scope', '$builder', '$validator', function($scope, $builder, $validator) {
          var arr = [];
          $.ajax({
              method: "GET",
              url: "http://localhost:3000/ViewAllFormsFilledByUser",
              success: function(data) {
                  for(var i=0;i<data.length;i++){
                      if(data[i] instanceof Object){
                          arr[i] = "<div><strong><a target='_blank' href='http://localhost:3000/GetFilledUserForm.html?filledFormId=" + data[i]._id+"'>http://localhost:3000/GetFilledUserForm.html?filledFormId=" + data[i]._id+"</a></strong></div><br />";
                      }
                  }
                  jQuery("#wrapper").html(arr);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                  console.log(textStatus, errorThrown);
              }
          });
      }
  ]);

}).call(this);

function getQueryVariable(getUrlParams) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        getUrlParams[pair[0]] = pair[1];
    }
    return getUrlParams;
}
