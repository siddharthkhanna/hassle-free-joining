var fs = require('fs');
var express = require('express');
var mongoose = require('mongoose');
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var db = mongoose.connection;

db.on('error', console.error);

db.once('open', function() {
    // Create your schemas and models here.
});

mongoose.connect('mongodb://localhost/test');

var movieSchema = new mongoose.Schema({
    //title: { type: String }
    //, rating: String
    //, releaseYear: Number
    //, hasCreditCookie: Boolean
    form: Object
});

var Movie = mongoose.model('Movie', movieSchema);

//app.get('/', function (req, res) {
//
//    var A = mongoose.model('A', schema);
//    console.log(req.body.dataform);
//});

app.post('/setForm',function(req, res){
    var thor = new Movie({
        //title: 'Thor'
        //, rating: 'PG-13'
        //, releaseYear: '2011'  // Notice the use of a String rather than a Number - Mongoose will automatically convert this for us.
        //, hasCreditCookie: true
        form: req.body.dataform
    });

    thor.save(function(err, thor) {
        if (err) return console.error(err);
        console.dir(thor);
    });
    console.log(req.body.dataform);
});


app.get('/getForm',function(req, res, next){
    Movie.findOne(function(err, thor) {
        if (err) return console.error(err);
        res.jsonp(thor);
    });
});

app.listen(1185);
console.log("server listening on 1185");