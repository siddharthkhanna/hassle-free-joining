var exp = require('express');
var app = exp();
var db = require('./db.js');
var bodyparser = require('body-parser');
var cookieParser = require('cookie-parser');
var crypto = require('crypto-js');
var secretKey = '123yut';
var shortid = require('shortid');
var mongoose = require('mongoose');
//var autoIncrement = require('mongoose-auto-increment');
var fs = require('fs');
var multer  = require('multer');
mongoose.connect('mongodb://127.0.0.1/test');
var conn = mongoose.connection;

var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;


app.use(cookieParser());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
  extended:true
}));

app.use(exp.static(__dirname+"/public",{ redirect : false }));

var dbMongo = mongoose.connection;

dbMongo.on('error', console.error);

dbMongo.once('open', function() {
    // Create your schemas and models here.
});

var connection = mongoose.connect('mongodb://localhost/test');

//autoIncrement.initialize(connection);
var recFormSchema = new mongoose.Schema({
    //title: { type: String }
    //, rating: String
    //, releaseYear: Number
    //, hasCreditCookie: Boolean
    uid: String,
    form: Object
});

//recFormSchema.plugin(autoIncrement.plugin, 'recForm');
var recForm = mongoose.model('recForm', recFormSchema);

var filledFormsSchema = new mongoose.Schema({
    UID: String,
    hrId: String,
    form: Object
});

var filledForms = mongoose.model('filledForms', filledFormsSchema);

app.get("/welcome/:token",function(req,res){
	res.send("Hello World");
});

app.get("/welcomeMail",function(req,res){
  res.send("Hello World");
  });


app.post("/registerUser",function(req,res){

var password = req.body.password;   

  var uuuid = shortid.generate();
   console.log(uuuid);
  console.log(req.body.username+"**");

  var username = req.body.username;
  var email = req.body.email;
  var type = req.body.Usertype;
  console.log("type"+type);
  var uid = crypto.HmacSHA1(uuuid,secretKey).toString();
  
  var userObj = {
    "uid":uid,
    "username":username,
    "emailid":email,
    "password":password,
    "userType":type
      };
      
  db.user.create(userObj)

  .then(function(user){
    console.log("Created");
    if(user){

        var mailer = require('./lib/sendMail');
        var from = "hasslefreeonboarding@gmail.com";
        var to = user.emailid;
        var url = "http://localhost:3000/jobseeker.html";
        var msg = "<html><body>Welcome to Onboarding Service. Ensuring hassle free on boarding for you.<br><br> You have been sucessfully registered as, <br><br> Username: "+user.username+"<br><p>Password: "+user.password+"</p><br>";
        var finalmag = msg+'<a href=\"'+url+"\">"+url.toString()+"</a></body></html>";
        console.log(finalmag);
        var msg = mailer.sendMail(to,from,finalmag);
         res.cookie('uid',user.uid,{
          maxAge:900000
        });
        res.send(user.uid);
    }
    else{
     console.log("dead");
    
    res.status(200);}
  })

  .catch(function(e){
    res.status(400).json(e);
    console.log(e);
  });
  

});

// app.get("/userData/:userid",function(req,res){
//  var userid = req.params.userid;
//   db.user.findOne({
//     where:{
//       uid:userid;
//     }
//   }).then(function(user){
//     res.send()
//   });
// });

app.post("/welcomeUserJ",function(req,res){
    console.log("Login Authentication");

    var username = req.body.username;
    var pasword = req.body.password;
    db.user.findOne({
      where:{
      emailid:username,
      password:pasword
    }
    })
    .then(function(user){
      console.log(user);
      if(user){
        res.cookie('user',user.username,{
          maxAge:900000
        });
         res.cookie('uid',user.uid,{
          maxAge:900000
        });
        res.cookie('type',user.userType,{
          maxAge:900000
        });
        
        res.redirect('MainPanel.html');


      }else{
        res.cookie('error','404',{
          maxAge:900000
        });
        res.redirect('jobseeker.html');
      }
    })
    .catch(function(e){
      res.status(500).send();
    });
    console.log("Auth Ended");
});



app.post("/sendMail",function(req,res){

  var mailer = require('./lib/sendMail');
  var from = "hasslefreeonboarding@gmail.com";
  var to = req.body.to;
  var url = req.body.formURL;
    console.log(url);
  if(url){
    MailforURL(to, url);
    res.redirect("http://localhost:3000/MainPanel.html#MailSent");
  }
  console.log(url);
  var uuid;
  var username;
  db.user.findOne({
      where:{
      emailid:to,
      userType:'1'
    }
    })
    .then(function(user){
      
      if(user){
        uuid = user.uid;
        username = user.username;  
        var tokenId = uuid +":"+username;
	      var uid = req.cookies.uid;
	      console.log(tokenId);
        tokenId = crypto.HmacSHA1(tokenId,secretKey).toString();
        //var bytes = crypto.AES.decrypt(tokenId,secretKey);
        //console.log(bytes.toString(crypto.enc.Utf8));
        var url = "http://localhost:3000/landingMail/" + tokenId+"/"+uid;
        var msg = "<html><body>Welcome to Onboarding Service. Ensuring hassle free on boarding for you.<br><br> User has uploaded his/her documents, <br><br>please link the below url to access documents now!<br><br>";
        var finalmag = msg+'<a href=\"'+url+"\">"+url.toString()+"</a></body></html>";
        console.log(finalmag);
        var msg = mailer.sendMail(to,from,finalmag);
	res.redirect('http://localhost:3000/MainPanel.html#MailSent');
      }else{        
        res.redirect('MainPanel.html#RecruiternotFound');
      }
    })
    .catch(function(e){
      res.status(500).send();
    });
    console.log("Auth Ended");  
});

function MailforURL(to,url){

  var mailer = require('./lib/sendMail');
  var from = "hasslefreeonboarding@gmail.com";
	url = url +"?key=1";
  var msg = "<html><body>Welcome to Onboarding Service. Ensuring hassle free on boarding for you.<br><br> Recruiter wants you to fill this form <br><br>please click the below url to fill the documents now!<br><br>";
  var finalmag = msg+'<a href=\"'+url+"\">"+url.toString()+"</a></body></html>";
  console.log(finalmag);
  var msg = mailer.sendMail(to,from,finalmag);
}

app.get("/landingMail/:tokenId/:uidMail",function(req,res){

var uid = req.cookies.uid;
var user = req.cookies.user;
console.log(uid);
console.log(user);
if(uid == 'undefined' || user == 'undefined'
  || typeof uid == 'undefined' || typeof user == 'undefined'){
  
    res.cookie('error','404',{
          maxAge:9000000
    });
     console.log("**********cookie empty**********");
     //res.sendFile(__dirname+"/public"+'/jobseeker.html');
     res.redirect('http://localhost:3000/jobseeker.html');
     
}
else{
var tokenidCook = uid+":"+user;
console.log(tokenidCook+"--------------------");
  var tokenId = req.params.tokenId;
  var bytes = crypto.HmacSHA1(tokenidCook,secretKey);
  console.log(bytes.toString()+"--------");
  if(bytes.toString() == tokenId.toString()){
    console.log("matched++++");
db.user.findOne({
      where:{
      uid:req.params.uidMail
      
    }
    }).then(function(user){

    console.log("-----mailer sucess------"+user.username);
    res.cookie('user',user.username,{
          maxAge:900000
    });
    res.cookie('uid',user.uid,{
          maxAge:900000
    });
    res.cookie('type','3',{
          maxAge:900000
    });
    res.redirect('http://localhost:3000/ViewDocs.html');

    }).catch(function(e){
      res.status(500).send();
    });
    
  }
  else
  {
    console.log("------bad request--------");
    res.cookie('user','undefined',{
          maxAge:900000
    });
    res.cookie('uid','undefined',{
          maxAge:900000
    });
    res.cookie('error','404',{
          maxAge:900000
    });
     res.redirect('http://localhost:3000/jobseeker.html');

  }
}
 console.log(" mail auth endede");
     
});


app.post('/setForm',function(req, res){
    var get_cookies = function(req) {
        var cookies = {};
        req.headers && req.headers.cookie.split(';').forEach(function(cookie) {
            var parts = cookie.match(/(.*?)=(.*)$/)
            cookies[ parts[1].trim() ] = (parts[2] || '').trim();
        });
        return cookies;
    };
    var uid = get_cookies(req)['uid'];
    console.log(req.body.dataform);
    var thor = new recForm({
        uid: uid,
        form: req.body.dataform
    });

    thor.save(function(err, thor) {
        if (err) return console.error(err);
        console.dir(thor);
    });
    console.log(req.body.dataform);
    res.send("aaaaaaaaaaa");
});


//############FILE UPLOAD############

var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, '/tmp/photo/');
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname + '-' + Date.now());
    }
});
var upload = multer({ storage : storage}).single('userPhoto');

function documentUpload(req){
    console.log('open');
    var metadata = {};
    var gfs = Grid(conn.db);
    // streaming to gridfs
    //filename to store in mongodb
    var get_cookies = function(req) {
        var cookies = {};
        req.headers && req.headers.cookie.split(';').forEach(function(cookie) {
            var parts = cookie.match(/(.*?)=(.*)$/);
            cookies[ parts[1].trim() ] = (parts[2] || '').trim();
        });
        return cookies;
    };
    var uid = get_cookies(req)['uid'];
    metadata['uid'] = uid;
    metadata['filename'] = req.file.filename;
    var writestream = gfs.createWriteStream({
        metadata: metadata,
        filename: uid
    });
    console.log(req.file.path);
    var filename = req.file.path;
    fs.createReadStream(filename).pipe(writestream);

    writestream.on('close', function (file) {
        // do something with `file`
        fs.unlink(req.file.path, function() {
        });
        console.log(file.filename + 'Written To DB');
    });
};

app.post('/api/photo',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            console.log(err);
            return res.end("Error uploading file.");
        }
        documentUpload(req);
        //res.end("File is uploaded");
        res.redirect('http://localhost:3000/formdisplay.html');
    });
});
//##########FILE UPLOAD##############
//########File Retrive###########
function documentRetrive(req,res){
    console.log('open');
    var gfs = Grid(conn.db);
    // streaming to gridfs
    //filename to store in mongodb
    //var get_cookies = function(req) {
    //    var cookies = {};
    //    req.headers && req.headers.cookie.split(';').forEach(function(cookie) {
    //        var parts = cookie.match(/(.*?)=(.*)$/)
    //        cookies[ parts[1].trim() ] = (parts[2] || '').trim();
    //    });
    //    return cookies;
    //};
    //var uid = get_cookies(req)['uid'];
    var fs_write_stream = fs.createWriteStream('/home/divyanshu/hassle-free-joining2/test.png');

//read from mongodb
    var readstream = gfs.createReadStream({
        filename: 'finalHomepageFlow.png-1453115684969'
    });
    readstream.pipe(fs_write_stream);
    console.log(readstream._grid.mongo.Chunk);
    fs_write_stream.on('close', function () {
        console.log('file has been written fully!');
    });
    //fs.createReadStream('/home/divyanshu/hassle-free-joining2/test.png').pipe(readstream);
//    console.log("aaaaaaaaaaaaaaaaaa" + readstream);
//    req.on('error', function(err) {
//        res.send(500, err);
//    });
//    readstream.on('error', function (err) {
//        res.send(500, err);
//    });
    res.send("done");
    //var result = readstream.pipe(res);
    //console.log(result);
    //res.send(res);
};
app.post('/api/retrieve',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            console.log(err);
            return res.end("Error uploading file.");
        }
        documentRetrive(req,res);

        res.end("File is uploaded");
        //res.redirect('http://localhost:3000/formdisplay.html');
    });
});

//#############file retrive##########
app.post('/insertUserForm',function(req,res){
    var uid = req.body.id;
    var hrId = req.body.hrId;
    dataform = req.body.dataform;
    fullformValues = req.body.fullformValues;
    var increment = 0;
    for (var i in dataform) {
        fullformValues[increment]['value'] = dataform[i].value;
        increment += 1;
    }
    var thor = new filledForms({
        UID: uid,
        hrId: hrId,
        form: JSON.stringify(fullformValues)
    });

    thor.save(function(err, thor) {
        if (err) return console.error(err);
        //console.dir(thor);
    });
    res.send();
});


app.get('/getFilledUserForm',function(req,res) {
    var uid = req.param('filledFormId');
    filledForms.find({_id: mongoose.Types.ObjectId(uid)}, 'form', function (err, result) {
        if (err)
            console.log('error occured in the database');
        res.jsonp(result[0].form);
    });
});

app.get('/getForm',function(req, res){
    recForm.find({_id: mongoose.Types.ObjectId(req.param('id'))},function(err, thor) {
        if (err) return console.error(err);
        console.log(thor[0]);
        res.jsonp(thor[0]);
    });
});

app.get('/showRecForm',function(req, res){
    var get_cookies = function(req) {
        var cookies = {};
        req.headers && req.headers.cookie.split(';').forEach(function(cookie) {
            var parts = cookie.match(/(.*?)=(.*)$/)
            cookies[ parts[1].trim() ] = (parts[2] || '').trim();
        });
        return cookies;
    };
    var uid = get_cookies(req)['uid'];
    recForm.find({uid: uid}, '_id, uid' ,function(err, thor) {
        if (err) return console.error(err);
        res.status(200).send(thor);
    });
});


db.sqlz.sync().then(function(){

  app.listen("3000",function(){
  console.log("Express is listening");
  });

});

app.get('/ViewAllFormsFilledByUser',function(req, res){
    var formsMadeByHr = [];
    var get_cookies = function(req) {
        var cookies = {};
        req.headers && req.headers.cookie.split(';').forEach(function(cookie) {
            var parts = cookie.match(/(.*?)=(.*)$/)
            cookies[ parts[1].trim() ] = (parts[2] || '').trim();
        });
        return cookies;
    };
    var uid = get_cookies(req)['uid'];
    recForm.find({uid: uid}, '_id' ,function(err, result) {
        if (err) return console.error(err);
        for (var i in result) {
            formsMadeByHr.push(result[i]._id);
        }
        filledForms.find({UID: {$in: formsMadeByHr}}, '_id, form', function (err, result) {
            if (err)
                console.log('error occured in the database');
            res.jsonp(result);
        });
    });
});

app.get('/api/viewAllUploadedDocs',function(req, res){
    var formsMadeByJobSeeker = [];
    var buffer = "";
    var get_cookies = function(req) {
        var cookies = {};
        req.headers && req.headers.cookie.split(';').forEach(function(cookie) {
            var parts = cookie.match(/(.*?)=(.*)$/);
            cookies[ parts[1].trim() ] = (parts[2] || '').trim();
        });
        return cookies;
    };
    var uid = get_cookies(req)['uid'];
    var gfs = Grid(conn.db);
    readStream = gfs.createReadStream({ "filename": "bbd43d133790059746399716873317697b3c2ec8" });
    readStream.on("data", function (chunk) {
        buffer += chunk;
    });
    readStream.on("end", function () {
        console.log("contents of file:\n\n", buffer);
    });
    //gfs.find({metadata: uid}, '_id' ,function(err, result) {
    //    if (err) return console.error(err);
    //    for (var i in result) {
    //        formsMadeByJobSeeker.push(result[i]._id);
    //    }
    //    console.log(formsMadeByJobSeeker);
    //    //filledForms.find({UID: {$in: formsMadeByJobSeeker}}, '_id, form', function (err, result) {
    //    //    if (err)
    //    //        console.log('error occured in the database');
    //    //    res.jsonp(result);
    //    //});
    //});
});
