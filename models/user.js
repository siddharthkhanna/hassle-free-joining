module.exports = function (sqlz, Datatypes){
	return sqlz.define('user',{
	uid:{
		type: Datatypes.STRING,
		allowNull:false,
		validate:{
			notEmpty: true
		}
	},
	username:{
		type: Datatypes.STRING,
		allowNull:false,
		validate:{
			notEmpty: true
		}
	},
	emailid:{
		type: Datatypes.STRING,
		allowNull:false,
		validate:{
			isEmail: true
		}
	},
	password:{
		type:Datatypes.STRING,
		allowNull:false,
		validate:{
			notEmpty:true
		}
	},
	userType:{
		type:Datatypes.INTEGER,
		allowNull:false,
		validate:{
			notEmpty:true
		}
	}
	
});

}